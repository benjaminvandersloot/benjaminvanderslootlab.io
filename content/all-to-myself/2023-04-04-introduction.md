+++
title = "What is the point of this blog?"
draft = true
+++

> But for the most part it is as solitary where I live as on the prairies. It is as much Asia or Africa as New England. I have, as it were, my own sun and moon and stars, and a little world all to myself. 
